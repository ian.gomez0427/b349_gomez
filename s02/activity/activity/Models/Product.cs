﻿using activity.Data;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace activity.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; } = null!;

        [Required]
        public Category Category { get; set; }

        [Required]
        public string Description { get; set; } = null!;

        [Required]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C0}")]
        public double Price { get; set; }

        [Required]
        [DataType(DataType.ImageUrl)] // Used to validate that the "Logo" field is a valid image url
        [Display(Name = "Product Image")]
        public string Logo { get; set; } = null!;

        
    }
}
