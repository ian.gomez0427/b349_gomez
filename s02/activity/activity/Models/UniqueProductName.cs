﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace activity.Models
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class UniqueProductNameAttribute : ValidationAttribute
    {
        private readonly string _actionName;

        public UniqueProductNameAttribute(string actionName)
        {
            _actionName = actionName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (_actionName.Equals("Create", StringComparison.OrdinalIgnoreCase))
            {
                // Perform uniqueness validation only for the "Create" action
                // Add your uniqueness validation logic here
            }

            // For other actions, or if no action is specified, allow the validation to pass
            return ValidationResult.Success;
        }
    }
}
