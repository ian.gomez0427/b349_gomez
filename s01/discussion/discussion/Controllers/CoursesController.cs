﻿using Microsoft.AspNetCore.Mvc;

namespace discussion.Controllers
{
    //CoursesController inherits from Controller (a base class provided by ASP.NET Core)
    public class CoursesController : Controller
    {
        // IActionResult represents the result of an action and can include views, data, or other responses.
        // Index() returns an IActionResult. Index() is the action method that is responsible for handling specific HTTP requests. In this case Index() is expected to handle requests to the Index page of the Courses section
        public IActionResult Index()
        {
            // The Index() returns a view as a result.
            // A view is responsible for rendering HTML content
            return View();
        }

        public IActionResult CreateCourse()
        {
            return View();
        }
    }
}
