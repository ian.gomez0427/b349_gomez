﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    public class Select
    {
        public void SelectOperator()
        {
            var courses = CourseDatabase.GetCourseData();

            //[SECTION] Method Based Syntax
            // The Select operator is used for selecting a particular property out of an object

            /*var data = courses.Select(course => course.Author);*/

            //[SECTION] Query Based Syntax
            var data = from c in courses select c.Author;


            foreach (var item in data)
            { 
                Console.WriteLine(item);
            }
        }

        public void SelectNameAndPrice() 
        {
            var courses = CourseDatabase.GetCourseData();

            // [SECTION] Method Based Syntax
            /*var data = courses.Select(course => new
            { 
                CourseName = course.Name,
                CoursePrice = course.Price
            });*/

            //[SECTION] Query Based Syntax
            /*var data = from course in courses 
                select new
                {
                    CourseName = course.Name,
                    CoursePrice = course.Price
                };*/

            /*var data2 = from c in courses select $"{c.Name} - Php {c.Price}";*/

            // Using tuples
            // Tuples provide a concise syntax to group multiple data elements in a lightweight data structure
            // We can work with the returned tuple instance directly or deconstruct it in separate variables as shown in the example below
            var data = courses.Select(c => (CourseName: c.Name, CoursePrice: c.Price)).ToList();

            foreach (var item in data)
            {
                Console.WriteLine($"{item.CourseName} - Php {item.CoursePrice}");
            }


        }
    }
}
