﻿using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace discussion.Models
{
    public class Course
    {
        [Key] //sets the Id as the primary key
        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 10, ErrorMessage = "The field must be between 10 and 50 characters.")] // Used to set the minimum and maximum characters of the form input
        public string Title { get; set; } = null!;

        [Required]
        public Stack Stack { get; set; }

        [Required]
        public string Description { get; set; } = null!;

        [Required]
        [DataType(DataType.Currency)] // Used to validate that the price input should be in currency value
        public double Price { get; set; }

        [Required]
        [DataType(DataType.Url)] // Used to validate that the stackURL is a valid link
        public string StackUrl { get; set; } = null!;

        [Required]
        [DataType(DataType.ImageUrl)] // Used to validate that the "Logo" field is a valid image url
        [Display(Name = "Logo")]

        public string Logo { get; set; } = null!;
    }
}
