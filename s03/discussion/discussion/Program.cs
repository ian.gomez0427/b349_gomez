﻿namespace discussion
{
    class Program
    {
        static void Main(string[] args)
        { 
            discussion.Select select = new discussion.Select();
            discussion.Where where = new discussion.Where();
            discussion.OrderByThenBy orderbythenby = new discussion.OrderByThenBy();
            discussion.GroupBy groupby = new discussion.GroupBy();

            /*select.SelectOperator();
            Console.WriteLine();
            select.SelectNameAndPrice();
            Console.WriteLine();
            where.WhereOperator();
            Console.WriteLine();
            orderbythenby.OrderByOperator();
            Console.WriteLine();
            orderbythenby.ThenByOperator();
            groupby.GroupByOperator();*/
            groupby.NewListBroupByOperator();
            groupby.MoreGroupBySample();

        }
    }
}