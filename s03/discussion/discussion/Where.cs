﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    public class Where
    {
        public void WhereOperator()
        {
            var courses = CourseDatabase.GetCourseData();

            // Method Based Syntax
            // The where operator is used to filter records that meet a specific condition
            /*var data = courses.Where(c => c.Price < 15000);*/

            // Query Based Syntax
            var data = from c in courses where c.Price < 15000 select c;

            foreach (var item in data)
            {
                Console.WriteLine($"{item.Name} - Php {item.Price}");
            }
        }
    }
}
